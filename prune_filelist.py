#!/usr/bin/env python3
import re
import argparse
import os.path


def prune_line(line):
    return re.sub(r'\(.+\)', '', line).strip()


def prune_filelist(raw_filelist, root=''):
    return (os.path.join(root, prune_line(line)) for line in raw_filelist if line)

def argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('filelist', help='File that contains files to prune. STDIN: -', type=argparse.FileType('r'), nargs='?', default='-')
    parser.add_argument('-r', '--root', help='Fileroot to prepend if files in filelist are relative to it', default='')
    return parser

def main():
    parser = argument_parser()
    args = parser.parse_args()

    with args.filelist as f:
        print(' '.join(prune_filelist(f.read().strip().splitlines(), args.root)))


if __name__ == '__main__':
    main()
