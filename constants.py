#!/usr/bin/env python3

def main():
    index = input('Start index: ')
    if not index:
        index = 0
    else:
        index = int(index)

    print('Type constant name, empty to print results and quit')

    results = []
    c = input()
    while c:
        results.append(c)
        c = input()

    print('const')
    for i, c in enumerate(results[:-1], index):
        print('    {} = {},'.format(c.upper(), i))

    print('    {} = {};'.format(results[-1].upper(), i + 1))

if __name__ == '__main__':
    main()
