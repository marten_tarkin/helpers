#!/usr/bin/env python

import sys
import csv

def parse_line(line):
    return [column.strip() for column in line.strip().split('|')]

def main():
    filename = sys.argv[1]

    with open(filename) as f:
        headers = parse_line(next(f))
        # Ignore separator
        next(f)

        lines = [parse_line(line) for line in f if line.strip()]

    # Remove row count
    if lines[-1][0].startswith('('):
        lines.pop()

    writer = csv.writer(sys.stdout, dialect='excel', delimiter=';')

    writer.writerow(headers)

    for line in lines:
        writer.writerow(line)


if __name__ == '__main__':
    main()
