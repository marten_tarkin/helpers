#!/usr/bin/env python3

import argparse
import shutil
import sys
from prune_filelist import prune_filelist

mail_template = """Ticket no: https://{trac_domain}/ticket/{ticket_nr}
Description: {description}
Orderer: {orderer}

List of files: {filelist}
"""

defaults = {
    'kv': {
        # 'trac_domain': 'trac.kv.dev',
        'trac_domain': '10.1.1.96',
        'orderer': 'Annett'
    },
    'osta': {
        # 'trac_domain': 'trac.osta.dev',
        'trac_domain': '10.1.1.97',
        'orderer': 'Siim'
    }
}

def parse_args():
    parser = argparse.ArgumentParser(description='Compose mail body for upload requests')
    parser.add_argument('name', choices=defaults.keys())
    parser.add_argument('ticket_nr', type=int)
    parser.add_argument('--orderer')
    parser.add_argument('-o', '--output', default='-')

    return parser.parse_args()

def main():
    args = parse_args()

    default_vars = defaults[args.name]

    desc = input('Description: ')

    print('Raw filelist (Ctrl+D for EOF):', file=sys.stderr)
    lines = [line.strip() for line in sys.stdin]

    fileroot = input('Fileroot: ').strip()

    filelist = prune_filelist(lines, fileroot)

    output_string = mail_template.replace('\n', '\r\n').format(
        trac_domain=default_vars['trac_domain'],
        ticket_nr=args.ticket_nr,
        description=desc,
        orderer=(args.orderer if args.orderer else default_vars['orderer']),
        filelist=' '.join(filelist)
    )

    print('-' * (shutil.get_terminal_size()[0] // 2), file=sys.stderr)
    print(file=sys.stderr)

    if args.output == '-':
        print(output_string)
    else:
        print(output_string)
        with open(args.output, 'w') as f:
            print(output_string, file=f)


if __name__ == '__main__':
    main()
