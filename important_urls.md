# Databases
## KV
db3.tln.ef.lan:5432

User: postgres

password:

## Osta
db1-pg96.tln.ef.lan:5432

User: postgres

password:


# Crons and monitoring
[Cron](https://tools.tix.ef.lan/cronmanager/)

[Cron logs](https://tools.tix.ef.lan/jobid/)

[Graylog monitoring](http://graylog.tix.ef.lan:9000/streams)


# Management
[Intranet](https://intranet.efadm.eu)

[SVN](http://code.ef.lan:18080/svn/)

[Trac](https://tracs.allepal.ee)

# Utilities
[Remote desktop Win7](win7.tln.ef.lan)
