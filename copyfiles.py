#!/usr/bin/env python3
# Extra arg is for file root, such as kv_fbx etc.
# 2nd arg is src root
# 3rd+ arg is dst root
import sys
import re
import shutil

if __name__ == '__main__':
    if len(sys.argv) >= 4:
        with open('filelist.txt') as f:
            for line in f.read().strip().splitlines():
                filepath = sys.argv[1] + re.sub(r'\(.+\)', '', line).strip()
                for dest in sys.argv[3:]:
                    print(shutil.copyfile(sys.argv[2] + filepath, dest + filepath))
    else:
        print("Missing parameter: file root")
