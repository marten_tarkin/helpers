#!/usr/bin/env python3
import intranet_client

interpeter = intranet_client.IntranetCmd()

commands = """
login marten
date 25
entry 0800 c24 other "meeting"
entry 0815 c24 "CITY24"
entry 1400 other "home"
""".strip().splitlines()

for c in commands:
    interpeter.onecmd(c)
