import requests
import json
import os.path

api_url = "https://api.city24.ee"
#api_url = "https://marten.api.dev.city24.ee"
indices = [
    # ('city24_realties', '20210616'),
    # ('city24_projects', '20210616'),
    # ('city24_modular_houses', '20210616'),
    # ('city24_brokers', '20210315'),
    # ('city24_locations', '20210315'),
    ('city24_companies', '20220921'),
]

def main():
    if os.path.exists('storage/tokens.json'):
        with open('storage/tokens.json') as f:
            tokens = json.load(f)
    else:
        tokens = None

    if tokens:
        r = requests.post(f'{api_url}/token/refresh',
            json={'refresh_token': tokens['refresh_token']},
            headers={'Accept': 'application/json', 'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:104.0) Gecko/20100101 Firefox/104.0'}
        )
    else:
        with open('storage/credentials.json') as f:
            r = requests.post(f'{api_url}/token',
                json=json.load(f),
                headers={'Accept': 'application/json', 'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:104.0) Gecko/20100101 Firefox/104.0'}
            )

    r.raise_for_status()
    with open('storage/tokens.json', 'w') as f:
        f.write(r.text)
    tokens = r.json()

    for index, version in indices:
        r = requests.get(
            f'{api_url}/_execute_command/elasticsearch:alias?alias={index}&version={version}',
            headers={'Authorization': f'Bearer {tokens["token"]}', 'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:104.0) Gecko/20100101 Firefox/104.0'}
        )
        print(r.text)



if __name__ == '__main__':
    main()
