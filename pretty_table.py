class PrettyTable:
    class Row(list):
        """Supported types: row, header, separator"""
        header_format = '{}'
        col_format = '{}'
        max_lengths = []
        column_buffers = (1, 3)

        def __init__(self, row, type='row', format_string=None, add_number=True):
            super().__init__(row)
            self.t = type
            self.format_string = format_string
            # Numbers only affect row types
            self._add_number = type == 'row' and add_number

        def get_format_string(self, default=None):
            _default = self.col_format if not default else default
            return self.format_string if self.format_string else _default

        def separator_string(self):
            if len(self.max_lengths) > 0:
                return '+'.join('-' * (max_len + self.column_buffers[0]) for max_len in self.max_lengths)
            return '-' * sum(self.column_buffers)

        def row_string(self, format_string):
            if len(self.max_lengths) > 0:
                return '|'.join(format_string.format(column, max_len=self.max_lengths[i]) for i, column in enumerate(self))
            return '|'.join(format_string.format(column, max_len=len(column) + self.column_buffers[1]) for column in self)

        def is_row(self):
            return self.t == 'row'

        def is_separator(self):
            return self.t == 'separator'

        def add_number(self):
            return self._add_number

        def __str__(self):
            if len(self.max_lengths) > 0:
                if self.t == 'separator':
                    return self.separator_string()
                elif self.t == 'header':
                    return self.row_string(self.get_format_string(self.header_format))

                return self.row_string(self.get_format_string())
            return super().__str__()


    def __init__(self, rows, header=None, column_buffers=(1, 3)):
        self.rows = [self.table_row(row) for row in rows]
        self.add_header(header)

        PrettyTable.Row.header_format = ' '*column_buffers[0] + '{:^{max_len}}'
        PrettyTable.Row.col_format = ' '*column_buffers[0] + '{:<{max_len}}'
        PrettyTable.Row.column_buffers = column_buffers

        self.reset_rows()

    def _add_operation(self, op_type, action):
        return self.operations.setdefault(op_type, action)

    def _add_numbers(self):
        if self.header:
            self.header.insert(0, '#')

        rows = []
        i = 1
        for row in self.rows:
            if row.is_row():
                if row.add_number():
                    row.insert(0, str(i))
                    i += 1
                else:
                    row.insert(0, '')
                rows.append(row)
            else:
                rows.append(row)

        self.rows = rows
        return self.rows

    def _calculate_max_lengths(self):
        rows = list(self.rows)
        if self.header:
            rows.insert(0, self.header)

        PrettyTable.Row.max_lengths = [max(len(row[i]) for row in rows if not row.is_separator()) + PrettyTable.Row.column_buffers[1] for i in range(len(rows[0]))]
        return PrettyTable.Row.max_lengths

    def _run_operation(self, op_type):
        if op_type in self.operations:
            self.operations[op_type]()

    def _compile_operations(self):
        self._run_operation('add_numbers')
        self._run_operation('calculate_max_lengths')
        self.operations.clear()

    def add_numbers(self):
        self._add_operation('add_numbers', self._add_numbers)
        return self

    def table_separator(self):
        return PrettyTable.Row([], type='separator')

    def table_row(self, row, **row_kwargs):
        return PrettyTable.Row(row, **row_kwargs)

    def add_row(self, row, **row_kwargs):
        self.rows.append(self.table_row(row, **row_kwargs))

    def add_header(self, header):
        self.header = PrettyTable.Row(header, type='header') if header else None

    def add_separator(self):
        self.rows.append(self.table_separator())

    def generate_table(self):
        self._compile_operations()

        rows = []
        if self.header:
            rows.append(self.header)
            rows.append(self.table_separator())

        rows.extend(self.rows)
        return rows

    def reset_rows(self):
        self.operations = {
            'calculate_max_lengths': self._calculate_max_lengths
        }

    def __str__(self):
        return '\n'.join(str(row) for row in self.generate_table())

    def __len__(self):
        return len(self.rows)

    def columns(self):
        if self.header:
            return len(self.header)
        elif len(self.rows) > 0:
            return len(self.rows[0])
        return 0

    def __iter__(self):
        return iter(self.rows)

if __name__ == '__main__':
    pt = PrettyTable([['test', 'texting stuff', 'not a problem at all', ''], ['lul', 'real', 'fml', 'real']]).add_numbers()
    pt.add_header(['tere', 'tere', 'vana', 'kere'])
    pt.add_separator()
    pt.add_row(['t', 't', 't', 't'])
    print(pt)
