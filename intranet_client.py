#!/usr/bin/env python3

import cmd
import datetime
import getpass
import requests
import shlex
import sys
import os.path
import re
from urllib.parse import urlparse, parse_qs
from bs4 import BeautifulSoup, SoupStrainer

from pretty_table import PrettyTable

try:
    import readline
except ImportError:
    print('Readline is not supported, line history disabled')

class MissingTableException(Exception): pass

class IntranetHTMLParser:
    def __init__(self, html_text=None):
        self.html_text = html_text

    def _init_parser(self, *args, **kwargs):
        return BeautifulSoup(self.html_text, 'html.parser', *args, **kwargs)

    def feed(self, html_text):
        self.html_text = html_text

    def read_entry_table(self):
        if not self.html_text:
            raise MissingTableException('No HTML loaded')

        bs = self._init_parser(parse_only=SoupStrainer('table', class_='tabelIse'))
        table = bs.find('table')
        if not table:
            raise MissingTableException('No table found in HTML')

        rows = []
        for tr in table.find_all('tr'):
            row = []
            for td in tr.find_all('td'):
                link = td.find('a')
                if link:
                    row.append({'text': link.string.strip(), 'href': link['href']})
                else:
                    row.append({'text': ''.join(td.stripped_strings)})
            rows.append(row)

        return rows

class Intranet:
    """
    Intranet handler

    Currently uses ~/.netrc for basic authentication to site
    """
    machine = 'intranet.efadm.eu'
    base_url = 'https://intranet.efadm.eu/index.php'

    categories = {
        'kv': 5, # Kinnisvara
        'kvn': 32, # KV (uus platv.)
        'c24': 34, # City24
        'osta': 8, # Osta
        'ostan': 33, # Osta (uus platv)
        'soov': 31, # Soov
        'allepal': 11, # AllePal
        'other': 6 # Töövaline
    }

    services = {
        'none': 0, # No service
        'prog': 1, # Application programming
        'dsup': 2, # Development support
        'other': 8 # Other
    }

    service_defaults = {
        'kv': 'prog',
        'kvn': 'prog',
        'c24': 'prog',
        'osta': 'prog',
        'ostan': 'prog',
        'soov': 'prog',
        'allepal': 'other',
        'other': 'none'
    }

    service_names = {
        'Application programming': 'prog',
        'Development support': 'dsup',
        'No service': 'none',
        'Other': 'other'
    }

    def __init__(self):
        self.session = requests.Session()
        self.logged_in = False
        self.current_date = datetime.date.today()

    def __enter__(self):
        return self

    def __exit__(self, *args):
        self.close()

    def close(self):
        if self.session:
            self.session.close()

    def set_basic_auth(self, username, password):
        self.session.auth = (username, password)

    def basic_auth_exists(self):
        return bool(self.session.auth)

    def login(self, username, password):
        params = {'op': 'login', 'username': username, 'password': password, 'login': 'Login'}
        r = self.session.post(self.base_url, data=params)
        self.logged_in = r.status_code == 200 and 'Sisselogimine ebaõnnestus!' not in r.text
        return r

    def logout(self):
        params = {'op': 'logout'}
        r = self.session.get(self.base_url, params=params)
        self.logged_in = self.logged_in and r.status_code != 200
        return r

    def get_index(self):
        return self.session.get(self.base_url)

    def set_date(self, date):
        self.current_date = date
        date_str = date.isoformat().replace('-', '')
        params = {'daction': 'switch_date', 'sdate': date_str}
        return self.session.get(self.base_url, params=params)

    def add_entry(self, time, category, service, description=''):
        params = {'op': 'insert_work', 'time': time, 'category_id': category, 'service_id': service, 'description': description, 'add': 'Add'}
        return self.session.get(self.base_url, params=params)

    def modify_entry(self, work_id, time, category, service, description=''):
        params = {'op': 'update_work', 'work_id': work_id, 'time': time, 'category_id': category, 'service_id': service, 'description': description, 'add': 'Modify'}
        return self.session.get(self.base_url, params=params)

    def delete_entry(self, work_id):
        params = {'op': 'delete', 'work_id': work_id}
        return self.session.get(self.base_url, params=params)


class IntranetCmd(cmd.Cmd):
    """
    Intranet handler as a commandline session
    """
    intro = 'Interface for accessing intranet via command-line'
    prompt = 'Intranet $ '
    prompt_default = 'Intranet {} $ '
    prompt_logged_in = 'Intranet {} ({}) $ '

    current_user = None
    html_parser = IntranetHTMLParser()

    weekday_strings = ('mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun')

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.intranet = Intranet()
        if not self.intranet.basic_auth_exists() and not self._check_netrc():
            user = input('Basic auth user: ')
            pw = getpass.getpass('Basic auth password: ')
            self.intranet.set_basic_auth(user, pw)

        self.max_lengths = {
            'categories': max(len(cat) for cat in self.intranet.categories),
            'services': max(len(serv) for serv in self.intranet.services)
        }
        self.entries = []

    def _check_netrc(self):
        with open(os.path.expanduser('~/.netrc')) as f:
            for line in f:
                if line.split()[1] == Intranet.machine:
                    return True
        return False

    def _set_prompt(self):
        date = self.intranet.current_date.isoformat()
        if self.current_user:
            self.prompt = self.prompt_logged_in.format(date, self.current_user)
        else:
            self.prompt = self.prompt_default.format(date)

    def precmd(self, line):
        if not any(line.startswith(command) for command in ['login', 'defaults', 'quit']):
            if not self.intranet.logged_in:
                print('Not logged in')
                return 'login'
        return line

    def postloop(self):
        self.intranet.close()

    def do_login(self, arg):
        """\
    Login user to Intranet. Must be called first before using other operations.
    Usage: login <user>"""
        if self.intranet.logged_in:
            print('Already logged in')
        else:
            username = arg if arg else input('Intranet username: ')
            pw = getpass.getpass('Intranet password: ')
            r = self.intranet.login(username, pw)

            if self.intranet.logged_in:
                self.current_user = username
                self._set_prompt()
                self._show_table(r.text)
            else:
                print('Try logging in again')

    def do_logout(self, arg):
        """\
    Logout user from Intranet.
    Usage: logout"""
        if self.intranet.logged_in:
            self.intranet.logout()

            if self.intranet.logged_in:
                print('Try again')
            else:
                self.current_user = None
                self._set_prompt()

    def _validate_entry(self, args):
        time, category = args[:2]
        if (time.endswith('h') or time.endswith('m')) and (len(time) < 2 or not time[:-1].isnumeric()):
            print('Time spent is incorrect. Examples: 5h; 30m')
        elif len(time) != 4 or not time.isnumeric():
            print('Time is incorrect. Example: 1330')
            return

        category_id = self.intranet.categories.get(category)
        if not category_id:
            print('No such category: {}. Possible categories: {}'.format(category, ', '.join(self.intranet.categories.keys())))
            return

        remaining_args = args[2:]

        if len(remaining_args) == 1:
            default = self.intranet.service_defaults.get(category)
            if not default:
                print('Service type must be provided. Possible services: {}'.format(', '.join(self.intranet.services.keys())))
                return

            service_id = self.intranet.services[default]
            desc = remaining_args[0]
        else:
            service, desc = remaining_args
            service_id = self.intranet.services.get(service)
            if not service_id:
                print('No such service: {}. Possible services: {}'.format(service, ', '.join(self.intranet.services.keys())))
                return

        return (time, category_id, service_id, desc)

    def _populate_table(self, text=''):
        if text:
            self.html_parser.feed(text)
        elif not self.html_parser.html_text:
            r = self.intranet.get_index()
            self.html_parser.feed(r.text)

        try:
            table = self.html_parser.read_entry_table()
        except MissingTableException:
            print('Session expired or does not exist. Retry last command after logging in')
            self.intranet.logged_in = False
            return None

        self.entries.clear()
        for i, row in enumerate(table):
            # 1st col contains work_id
            linked_time = row[0]
            if 'href' in linked_time:
                query = parse_qs(urlparse(linked_time['href']).query)
                if 'work_id' in query:
                    self.entries.append(query['work_id'][0])
                else:
                    print('Work id not present for row', i)
                    self.entries.append(0)

        return table

    def _print_table(self, table):
        if len(table) > 0:
            text_table = [[col['text'] for col in row] for row in table]
            pretty_table = PrettyTable(text_table[1:], header=text_table[0]).add_numbers()

            if len(text_table) > 1:
                summary_row = ['']*(pretty_table.columns() - 2)
                summary_row.append('Work hours:')
                summary_row.append(str(sum(float(re.match('^\\s*([0-9\.]+).*$', row[-1]).group(1)) for row in pretty_table if row[-1] and Intranet.service_names.get(row[2], 'none') != 'none')) + 'h')
                pretty_table.add_separator()
                pretty_table.add_row(summary_row, add_number=False)

            print()
            print(pretty_table)
            print()

    def _show_table(self, text=''):
        table = self._populate_table(text)
        if table is None:
            return
        self._print_table(table)

    def do_defaults(self, arg):
        """\
    Show default services for each category. Usage: defaults [<category>]"""
        flipped_service_names = {v: k for k, v in self.intranet.service_names.items()}
        if not arg:
            print('Services:')
            print('\n'.join('{}: {}'.format(cat, flipped_service_names[serv]) for cat, serv in self.intranet.service_defaults.items()))
        else:
            cat = shlex.split(arg)[0]
            if cat in self.intranet.service_defaults:
                print('{}: {}'.format(cat, flipped_service_names[self.intranet.service_defaults[cat]]))
            else:
                print('No such category:', cat)

    def do_entry(self, arg):
        """\
    Add entry to current date.
    If service or category is not provided, a default will be used. Defaults can be read with command: defaults
    Usage: entry <time> <category> <service> <description>
           entry <time> <category> <description>
           entry <time> <description>"""
        arg_list = shlex.split(arg)
        if len(arg_list) < 3:
            print('Not enough arguments')
            return

        args = self._validate_entry(arg_list)

        if args:
            if args[0].endswith('h'):
                spent_hours = args[:-1]
            r = self.intranet.add_entry(*args)
            if r.status_code == 200:
                self._show_table(r.text)
            else:
                print('Failed to add entry')

    def do_modify(self, arg):
        """\
    Modify entry on current date.
    If service is not provided, a default will be used. Defaults can be read with command: defaults
    <nr> is strictly positive integer.
    Usage: modify <nr> <time> <category> [<service>] <description>"""
        arg_list = shlex.split(arg)
        if len(arg_list) < 4:
            print('Not enough arguments')
            return

        nr, entry_args = arg_list[0], arg_list[1:]

        if not nr.isnumeric():
            print('First argument must be numeric')
            return

        if int(nr) > len(self.entries):
            print('Number is too big, there are a total of {} entries.'.format(len(self.entries)))
            return

        work_id = self.entries[int(nr) - 1]

        args = self._validate_entry(entry_args)

        if args:
            r = self.intranet.modify_entry(work_id, *args)
            if r.status_code == 200:
                self._show_table(r.text)
            else:
                print('Failed to modify entry')

    def do_delete(self, arg):
        """\
    Delete entry on current date.
    <nr> is strictly positive integer. Use the first column on the table to delete selected row
    Usage: delete <nr>"""
        nr = arg

        if not nr.isnumeric():
            print('First argument must be numeric')
            return

        if int(nr) > len(self.entries):
            print('Number is too big, there are a total of {} entries.'.format(len(self.entries)))
            return

        work_id = self.entries[int(nr) - 1]

        r = self.intranet.delete_entry(work_id)
        if r.status_code == 200:
            self._show_table(r.text)
        else:
            print('Failed to delete entry')

    def _parse_weekday(self, from_date, string):
        weekday = from_date.weekday()
        target_weekday = self.weekday_strings.index(string)
        day_diff = (weekday - target_weekday) % len(self.weekday_strings)
        return from_date - datetime.timedelta(days=day_diff if day_diff > 0 else len(self.weekday_strings))

    def do_date(self, arg):
        """\
    Change current date. No arguments sets to current date.
    Special values for <day>: tomorrow, t, yesterday, y, mon, tue, wed, thu, fri, sat, sun
    Usage: date [<day> [<month> [<year>]]]"""
        date_parts = shlex.split(arg)
        new_date = datetime.date.today()
        if len(date_parts) == 1:
            day_part = date_parts[0].lower()
            if day_part in ('t', 'tomorrow'):
                new_date += datetime.timedelta(days=1)
            elif day_part in ('y', 'yesterday'):
                new_date -= datetime.timedelta(days=1)
            elif day_part in self.weekday_strings:
                new_date = self._parse_weekday(new_date, day_part)
            elif day_part[0] in ('+', '-'):
                new_date += datetime.timedelta(days=int(day_part))
            else:
                new_date = new_date.replace(day=int(day_part))
        elif len(date_parts) == 2:
            new_date = new_date.replace(day=int(date_parts[0]), month=int(date_parts[1]))
        elif len(date_parts) == 3:
            new_date = new_date.replace(day=int(date_parts[0]), month=int(date_parts[1]), year=int(date_parts[2]))

        r = self.intranet.set_date(new_date)
        if r.status_code == 200:
            self._show_table(r.text)
            self._set_prompt()
        else:
            print('Failed to change date')

    def do_table(self, arg):
        """\
    Print current entry table.
    Usage: table"""
        self._show_table()

    def do_quit(self, arg):
        """\
    Closes connection and exits interpreter.
    Usage: quit"""
        return True


def main():
    interpreter = IntranetCmd()
    if len(sys.argv) > 1:
        interpreter.onecmd('login ' + sys.argv[1])
    interpreter.cmdloop()

if __name__ == '__main__':
    main()
