#!/usr/bin/env python3
import argparse
import datetime
import json
import re
import sys
import urllib.request

# from crontab import CronTab


# def parse_schedule(schedule_str: str) -> CronTab:
#     parsed_schedule = schedule_str
#     if schedule_str.startswith('@every'):
#         match = re.match(r'@every ([0-9]+)([mh])', schedule_str)
#         if not match:
#             raise Exception(f'Could not determine schedule of {schedule_str}')

#         amount = match.group(1)
#         if match.group(2) == 'm':
#             return CronTab(f'*/{amount} * * * *')

#         if match.group(2) == 'h':
#             return CronTab(f'* */{amount} * * *')

#     if not schedule_str.startswith('@'):
#         values = schedule_str.split(' ')
#         if values[2] == '0':
#             values[2] = '1'
#         if values[3] == '0':
#             values[3] = '1'
#         if values[5] == '0':
#             values[5] = '-'
#         return CronTab(' '.join(values))

#     return CronTab(parsed_schedule)


def parse_iso_string(date_string: str) -> datetime.datetime:
    no_tz = date_string.rstrip('Z')
    split_ms = no_tz.split('.')
    if len(split_ms) > 1:
        date_and_time = split_ms[0]
        ms = split_ms[1][:6]
        return datetime.datetime.strptime(f'{date_and_time}.{ms}', '%Y-%m-%dT%H:%M:%S.%f')

    return datetime.datetime.strptime(no_tz, '%Y-%m-%dT%H:%M:%S')


def argument_parser():
    parser = argparse.ArgumentParser(description='Checks for late-running jobs on Dkron')
    parser.add_argument('excluded_jobs', nargs='*', help='Regex patterns for jobs to exclude from checking')
    parser.add_argument('--api-url', nargs=1, help='Dkron API URL. [http://crons.tix.ef.lan/v1]', default='http://crons.tix.ef.lan/v1')
    return parser


EXIT_SUCCESS = 0
EXIT_WARNING = 1
EXIT_DOWN_CRITICAL = 2
EXIT_DOWN_UNKNOWN = 3

def main():
    parser = argument_parser()
    args = parser.parse_args()
    with urllib.request.urlopen(args.api_url + '/jobs') as r:
        jobs = json.load(r)

    ret_code = EXIT_SUCCESS
    current_dt = datetime.datetime.now()
    current_utc = datetime.datetime.utcfromtimestamp(current_dt.timestamp())
    offset = current_dt - current_utc
    offset_min = offset / datetime.timedelta(minutes=1)
    tz_string = '{}{:02}:{:02}'.format('+' if offset_min > 0 else '-', int(offset_min // 60), int(offset_min % 60))

    job_patterns = [re.compile(pattern, re.IGNORECASE) for pattern in args.excluded_jobs]

    for job in jobs:
        ignore_job = False
        for pattern in job_patterns:
            if pattern.search(job['name']):
                ignore_job = True
                break

        if ignore_job:
            print('{} is ignored.'.format(job['name']))
            continue
        if job['disabled']:
            print('{} is disabled!'.format(job['name']))
            continue
        if not job['last_success']:
            continue

        last_success = parse_iso_string(job['last_success'])
        next_planned = parse_iso_string(job['next'])

        if next_planned < last_success or next_planned > current_utc:
            continue

        next_planned_offset = current_utc - next_planned
        if next_planned_offset > datetime.timedelta(minutes=5):
            with urllib.request.urlopen(args.api_url + '/jobs/{job_name}/executions'.format(job_name=job['name'])) as r:
                executions = json.load(r)

            running_ex = next((ex for ex in executions if ex['finished_at'] == '0001-01-01T00:00:00Z'), None)
            if running_ex and next_planned_offset > datetime.timedelta(minutes=30):
                started_at = parse_iso_string(running_ex['started_at'])
                started_at_local = started_at + offset
                print('{} is still running after 30 minutes! Started at {}{}'.format(
                    job['name'], started_at_local.isoformat(), tz_string), file=sys.stderr)
                ret_code = EXIT_WARNING
                continue

            last_success_local = last_success + offset
            print('{} is over 5 minutes late! Last success: {}{}'.format(job['name'], last_success_local.isoformat(), tz_string), file=sys.stderr)
            ret_code = EXIT_WARNING

    return ret_code

if __name__ == "__main__":
    sys.exit(main())
